const express = require('express');
const usersOriginal = require('../data/users');
const { check, validationResult } = require('express-validator/check');

module.exports = app => {
  let usersCopy = [ ...usersOriginal ];
  let userCount = usersCopy.length;
  const router = express.Router();
  
  router.delete('/food/:name', (req, res) => {
    try {
      usersCopy = usersCopy.filter(user => user.hero_name !== req.params.name);
      res.json({
        success: true
      })
    } catch (err) {
      res.status(422).send(err.toString());
    }
  });

  router.get('/foods', (req, res) => {
    const sorted = usersCopy.sort((a, b) => a.id - b.id);
    // Combine Duplicate Hero
    const result = Object.values(sorted.reduce((newItem,currentValue) => {
      newItem[currentValue.hero_name] = newItem[currentValue.hero_name] || {
          
          'hero_name': currentValue.hero_name, 
          'first_name': currentValue.first_name, 
          'last_name': currentValue.last_name, 
          'favorite_foods' : []};

      newItem[currentValue.hero_name]['favorite_foods']
      .push({'id' : currentValue.id,"food":currentValue.favorite_food});
      return newItem;
    }, {}));
    res.send(result);
  });
  
  router.post('/food', [
    check('hero_name').exists(),
    check('first_name').exists(),
    check('last_name').exists()
  ],(req, res) => {
    try {
      validationResult(req).throw();
      userCount += 1;
      usersCopy.push({
        id: userCount,
        ...req.body
      })
      res.json({
        success: userCount
      })
    } catch (err) {
      res.status(422).send(err.toString());
    }
  });

  app.use('/users', router);
}